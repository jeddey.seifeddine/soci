<?php

/**
 * @param int $i
 */
function FizzBuzz($i = 100): void
{
    for ($j = 0 ; $j <= $i ; $j++) {
        switch($j) {
            case $j % 3 == 0 && $j % 5 == 0:
                echo 'FizzBuzz'.PHP_EOL;
                break;
            case $j % 3 == 0:
                echo 'Fizz'.PHP_EOL;
                break;
            case $j % 5 == 0:
                echo 'Buzz'.PHP_EOL;
                break;
            default:
                echo $j.PHP_EOL;
                break;
        }
    }
}

FizzBuzz()

?>