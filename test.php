<?php
require_once 'vendor/autoload.php';

use BookShelf\Book\Book;
use BookShelf\BookShelf;
use BookShelf\Config\BookShelfConfig;

$config = new BookShelfConfig(3);
$shelf = new BookShelf($config);
$shelf->store('BOOKID', new Book('Book Name','Book Author'));
$myBook = $shelf->retreive('BOOKID');
echo 'Page Content : ' . $myBook->getContentByPageNumber(3). PHP_EOL;
echo $myBook->getAuthor();
