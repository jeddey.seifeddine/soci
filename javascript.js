var data = [
    {
        "page": 1,
        "totalPages": 5,
        "data": [
            {
                "title": "Movie 1",
                "rating": 4.7
            }, {
                "title": "Movie 2",
                "rating": 7.9
            }],
    },
    {
        "page": 2,
        "totalPages": 5,
        "data": [
            {
                "title": "Movie 3",
                "rating": 5.1
            }, {
                "title": "Movie 4",
                "rating": 2.4
            }],
    }
];
var result = [];

data.forEach(function(item,index) {

    var element = item.data;
    var info = [] ;
    info.pageNumber = item.page;
    info.countDataset = element.length;

    var maximumDataSetRating = 0 ;
    var maximumDataSetRatingMovieName ;
    var sumTotalRatingByDataSet = 0;
    element.forEach(function(item,i) {

        sumTotalRatingByDataSet += item.rating;
        if(maximumDataSetRating < item.rating) {
            maximumDataSetRating = item.rating;
            maximumDataSetRatingMovieName = item.title
        }

        info.maximumDataSetRating = maximumDataSetRating;
        info.maximumDataSetRatingMovieName = maximumDataSetRatingMovieName;
    });
    info.dataSetAverageRating = (parseInt(sumTotalRatingByDataSet) / element.length);
    result.push(info)
});

console.log(result);


/**:
 *Array(
        countDataset: 2
        dataSetAverageRating: 6
        maximumDataSetRating: 7.9
        maximumDataSetRatingMovieName: "Movie 2"
        pageNumber: 1
)

 Array(
        countDataset: 2
        dataSetAverageRating: 3.5
        maximumDataSetRating: 5.1
        maximumDataSetRatingMovieName: "Movie 3"
        pageNumber: 2
 **/