SELECT COUNT(p.id) as TOTAL_POST_NUMBER FROM posts as p
INNER JOIN users as u
ON u.id = p.user_id
WHERE u.email = 'test@gmail.com'
AND created_at > now() - INTERVAL 30 day;

###########################################################################

what kind of index would you add to the `posts` table to make this query work efficiently.

# Setting Non clustered index on user_id column

