<?php

namespace BookShelf\Book;

use BookShelf\Base\BaseBook;
use BookShelf\Interfaces\BookInterface;

class Magazine extends BaseBook implements BookInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $content = [
        1 => 'content page 1',
        2 => 'content page 2',
        3 => 'content page 3',
        4 => 'content page 4',
        5 => 'content page 5',
    ];

    /**
     * Magazine constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int $pageNumber
     * @return string
     */
    public function getContentByPageNumber(int $pageNumber): string
    {
        return parent::getContentByPageNumber($pageNumber);
    }
}