<?php

namespace BookShelf\Book;

use BookShelf\Base\BaseBook;
use BookShelf\Interfaces\BookInterface;

class Book extends BaseBook implements BookInterface
{

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var array
     */
    protected $content = [
      1 => 'content page 1',
      2 => 'content page 2',
      3 => 'content page 3',
      4 => 'content page 4',
      5 => 'content page 5',
    ];

    /**
     * Book constructor.
     * @param string $title
     * @param string $author
     */
    public function __construct(string  $title, string $author)
    {
        $this->title = $title;
        $this->author = $author;

    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @param int $pageNumber
     * @return string
     */

    public function getContentByPageNumber(int $pageNumber): string
    {
       return parent::getContentByPageNumber($pageNumber);
    }
}