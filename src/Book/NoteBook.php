<?php

namespace BookShelf\Book;

use BookShelf\Base\BaseBook;
use BookShelf\Interfaces\BookInterface;

class NoteBook extends BaseBook implements BookInterface
{

    /**
     * @var string
     */
    protected $owner;

    /**
     * @var array
     */
    protected $content = [
        1 => 'content page 1',
        2 => 'content page 2',
        3 => 'content page 3',
        4 => 'content page 4',
        5 => 'content page 5',
    ];

    /**
     * NoteBook constructor.
     * @param string $owner
     */
    public function __construct(string $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     */
    public function setOwner(string $owner): void
    {
        $this->owner = $owner;
    }


    /**
     * @param int $pageNumber
     * @return string
     */
    public function getContentByPageNumber(int $pageNumber): string
    {
        return parent::getContentByPageNumber($pageNumber);
    }
}