<?php

namespace  BookShelf;

use BookShelf\Exception\CapacityException;
use BookShelf\Interfaces\BookInterface;
use BookShelf\Interfaces\BookShelfConfigInterface;


class BookShelf
{
    /**
     * @var BookShelfConfigInterface
     */
    protected $config;

    /**
     * @var array
     */
    protected $shelfStore = [];

    public function __construct(BookShelfConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @param String $id
     * @param BookInterface $book
     * @throws CapacityException
     */
    public function store(String $id,BookInterface $book): void
    {
        if (!$this->getAvailableShelfCapacity() > 0) {
             throw new CapacityException();
        }

        if (isset($this->shelfStore[$id])) {
            throw new AlreadyExistException();
        }
        $this->shelfStore[$id] = $book;
    }


    /**
     * @param string $id
     * @return mixed|null
     */
    public function retreive(string  $id)
    {
        return isset($this->shelfStore[$id]) ? $this->shelfStore[$id] : null;
    }

    /**
     * @return int
     */
    public function getShelfCurrentCapacity()
    {
        return count($this->shelfStore);
    }

    /**
     * @return int
     */
    public function getAvailableShelfCapacity(): int
    {
        $avalaibleCapacity = 0 ;
        $currentShelfCapacity = $this->getShelfCurrentCapacity();
        $shelfMaxCapacity = $this->config->getCapacity();
        if($shelfMaxCapacity > $currentShelfCapacity) {
            $avalaibleCapacity = $shelfMaxCapacity - $currentShelfCapacity;
        }
        return $avalaibleCapacity;

    }

}