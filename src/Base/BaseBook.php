<?php

namespace BookShelf\Base;

class BaseBook
{
    public function getContentByPageNumber(int $pageNumber): string
    {
        if (key_exists($pageNumber, $this->content)){
            return $this->content[$pageNumber];
        }
        return "page doesn't exist" ;
    }
}