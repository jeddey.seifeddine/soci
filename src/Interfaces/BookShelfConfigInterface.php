<?php
/**
 * Created by PhpStorm.
 * User: jeddey.seifeddine
 * Date: 5/2/2021
 * Time: 3:49 PM
 */

namespace BookShelf\Interfaces;


interface BookShelfConfigInterface
{
    public function getCapacity(): int;
}