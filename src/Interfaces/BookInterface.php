<?php

namespace BookShelf\Interfaces;

/**
 * Interface BookInterface
 * @package BookShelf\Interfaces
 */
interface BookInterface
{
    public function getContentByPageNumber(int $pageNumber): string;
}