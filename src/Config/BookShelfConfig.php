<?php

namespace BookShelf\Config ;
use BookShelf\Interfaces\BookShelfConfigInterface;


class BookShelfConfig implements BookShelfConfigInterface
{

    /**
     * @var int
     */
    protected $capacity;

    /**
     * BookShelfConfig constructor.
     * @param int $capacity
     */
    public function __construct(int $capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }
}