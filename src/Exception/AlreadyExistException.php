<?php

namespace BookShelf\Exception;

use Exception;

class AlreadyExistException extends Exception
{
    public $message = 'Already Exist Book In Shelf';
}
