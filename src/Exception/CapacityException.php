<?php

namespace BookShelf\Exception ;

use Exception;

class CapacityException extends Exception
{
    public $message = 'No More Capacity Left in the Shelf';
}